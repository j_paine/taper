<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dose;

/**
 * DoseSearch represents the model behind the search form about `app\models\Dose`.
 */
class DoseSearch extends Dose
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pill_id', 'taper_id', 'user_id', 'date', 'created_at', 'updated_at'], 'integer'],
            [['weight'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dose::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'pill_id' => $this->pill_id,
            'taper_id' => $this->taper_id,
            'user_id' => $this->user_id,
            'weight' => $this->weight,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
