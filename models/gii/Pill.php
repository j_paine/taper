<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "pill".
 *
 * @property integer $id
 * @property string $name
 * @property double $weight
 * @property double $empty_weight
 * @property double $dose
 * @property integer $drug_id
 * @property integer $default
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Dose[] $doses
 * @property Drug $drug
 */
class Pill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'weight', 'dose', 'drug_id', 'created_at'], 'required'],
            [['weight', 'empty_weight', 'dose'], 'number'],
            [['drug_id', 'default', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Weight'),
            'empty_weight' => Yii::t('app', 'Empty Weight'),
            'dose' => Yii::t('app', 'Dose'),
            'drug_id' => Yii::t('app', 'Drug ID'),
            'default' => Yii::t('app', 'Default'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoses()
    {
        return $this->hasMany(Dose::className(), ['pill_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrug()
    {
        return $this->hasOne(Drug::className(), ['id' => 'drug_id']);
    }
}
