<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "taper".
 *
 * @property integer $id
 * @property integer $drug_id
 * @property double $start_dose
 * @property double $end_dose
 * @property double $step_size
 * @property integer $step_freq
 * @property integer $start_date
 * @property integer $end_date
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Dose[] $doses
 * @property Drug $drug
 */
class Taper extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taper';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['drug_id', 'start_dose', 'end_dose', 'step_size', 'start_date', 'created_at'], 'required'],
            [['drug_id', 'step_freq', 'start_date', 'end_date', 'status', 'created_at', 'updated_at'], 'integer'],
            [['start_dose', 'end_dose', 'step_size'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'drug_id' => Yii::t('app', 'Drug ID'),
            'start_dose' => Yii::t('app', 'Start Dose'),
            'end_dose' => Yii::t('app', 'End Dose'),
            'step_size' => Yii::t('app', 'Step Size'),
            'step_freq' => Yii::t('app', 'Step Freq'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoses()
    {
        return $this->hasMany(Dose::className(), ['taper_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrug()
    {
        return $this->hasOne(Drug::className(), ['id' => 'drug_id']);
    }
}
