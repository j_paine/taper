<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "dose".
 *
 * @property integer $id
 * @property integer $pill_id
 * @property integer $taper_id
 * @property integer $user_id
 * @property double $weight
 * @property integer $date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property Pill $pill
 * @property Taper $taper
 */
class Dose extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dose';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pill_id', 'taper_id', 'user_id', 'weight', 'date', 'created_at'], 'required'],
            [['pill_id', 'taper_id', 'user_id', 'date', 'created_at', 'updated_at'], 'integer'],
            [['weight'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pill_id' => Yii::t('app', 'Pill ID'),
            'taper_id' => Yii::t('app', 'Taper ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'weight' => Yii::t('app', 'Weight'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPill()
    {
        return $this->hasOne(Pill::className(), ['id' => 'pill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaper()
    {
        return $this->hasOne(Taper::className(), ['id' => 'taper_id']);
    }
}
