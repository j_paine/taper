<?php

namespace app\models\gii;

use Yii;

/**
 * This is the model class for table "drug".
 *
 * @property integer $id
 * @property string $name
 * @property string $nick
 * @property double $halflife_low
 * @property double $halflife_high
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property Pill[] $pills
 * @property Taper[] $tapers
 */
class Drug extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drug';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'user_id', 'created_at'], 'required'],
            [['halflife_low', 'halflife_high'], 'number'],
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'nick'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'nick' => Yii::t('app', 'Nick'),
            'halflife_low' => Yii::t('app', 'Halflife Low'),
            'halflife_high' => Yii::t('app', 'Halflife High'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPills()
    {
        return $this->hasMany(Pill::className(), ['drug_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTapers()
    {
        return $this->hasMany(Taper::className(), ['drug_id' => 'id']);
    }
}
