<?php

namespace app\models;

use \Yii;
use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;

class Drug extends gii\Drug {
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['halflife_low', 'halflife_high'], 'number'],
            [['name', 'nick'], 'string', 'max' => 255]
        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'nick' => Yii::t('app', 'Alias'),
            'halflife_low' => Yii::t('app', 'Half-life Min'),
            'halflife_high' => Yii::t('app', 'Half-life Max'),
            'user_id' => Yii::t('app', 'User'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            UserBehavior::className(),
            TimestampBehavior::className()
        ];
    }

}
