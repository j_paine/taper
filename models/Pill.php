<?php

namespace app\models;

use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;

class Pill extends gii\Pill {
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'weight', 'dose'], 'required'],
            [['weight', 'empty_weight', 'dose'], 'number'],
            [['default'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Weight'),
            'empty_weight' => Yii::t('app', 'Empty Weight'),
            'dose' => Yii::t('app', 'Dose'),
            'drug_id' => Yii::t('app', 'Drug'),
            'default' => Yii::t('app', 'Default Pill'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            UserBehavior::className(),
            TimestampBehavior::className()
        ];
    }

}
