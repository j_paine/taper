<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Taper;

/**
 * TaperSearch represents the model behind the search form about `app\models\Taper`.
 */
class TaperSearch extends Taper
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'drug_id', 'step_freq', 'start_date', 'end_date', 'status', 'created_at', 'updated_at'], 'integer'],
            [['start_dose', 'end_dose', 'step_size'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Taper::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'drug_id' => $this->drug_id,
            'start_dose' => $this->start_dose,
            'end_dose' => $this->end_dose,
            'step_size' => $this->step_size,
            'step_freq' => $this->step_freq,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
