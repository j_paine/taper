<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pill;

/**
 * PillSearch represents the model behind the search form about `app\models\Pill`.
 */
class PillSearch extends Pill
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'drug_id', 'default', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
            [['weight', 'empty_weight', 'dose'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pill::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'weight' => $this->weight,
            'empty_weight' => $this->empty_weight,
            'dose' => $this->dose,
            'drug_id' => $this->drug_id,
            'default' => $this->default,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
