<?php

namespace app\models;

use \Yii;
use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;

class Dose extends gii\Dose {

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weight'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pill_id' => Yii::t('app', 'Pill'),
            'taper_id' => Yii::t('app', 'Taper'),
            'user_id' => Yii::t('app', 'User'),
            'weight' => Yii::t('app', 'Weight'),
            'date' => Yii::t('app', 'Date'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            UserBehavior::className(),
            TimestampBehavior::className()
        ];
    }
}
