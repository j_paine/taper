<?php

namespace app\models;

use app\components\behaviors\UserBehavior;
use yii\behaviors\TimestampBehavior;

class Taper extends gii\Taper {

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_dose', 'end_dose', 'step_size', 'start_date'], 'required'],
            [['step_freq', 'start_date', 'end_date', 'status'], 'integer'],
            [['start_dose', 'end_dose', 'step_size'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'drug_id' => Yii::t('app', 'Drug'),
            'start_dose' => Yii::t('app', 'Start Dose'),
            'end_dose' => Yii::t('app', 'End Dose'),
            'step_size' => Yii::t('app', 'Step Size'),
            'step_freq' => Yii::t('app', 'Step Frequency'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created'),
            'updated_at' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            UserBehavior::className(),
            TimestampBehavior::className()
        ];
    }
}
