<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_130814_create_taper_table extends Migration
{
    public function up()
    {
        $this->createTable('taper', [
            'id' => Schema::TYPE_PK,
            'drug_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'start_dose' => Schema::TYPE_FLOAT . ' NOT NULL',
            'end_dose' => Schema::TYPE_FLOAT . ' NOT NULL',
            'step_size' => Schema::TYPE_FLOAT . ' NOT NULL',
            'step_freq' => Schema::TYPE_INTEGER,
            'start_date' => Schema::TYPE_INTEGER . ' NOT NULL',
            'end_date' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->addForeignKey('fk_taper_drug_drug', 'taper', 'drug_id', 'drug', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('taper');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
