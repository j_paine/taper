<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_130448_create_pill_table extends Migration
{
    public function up()
    {
        $this->createTable('pill', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'weight' => Schema::TYPE_FLOAT . ' NOT NULL',
            'empty_weight' => Schema::TYPE_FLOAT,
            'dose' => Schema::TYPE_FLOAT . ' NOT NULL',
            'drug_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'default' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
        $this->addForeignKey('fk_pill_drug_drug', 'pill', 'drug_id', 'drug', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('pill');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
