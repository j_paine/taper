<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_141407_create_dose_table extends Migration
{
    public function up()
    {
        $this->createTable('dose', [
            'id' => Schema::TYPE_PK,
            'pill_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'taper_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'weight' => Schema::TYPE_FLOAT . ' NOT NULL',
            'date' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey('fk_dose_pill_pill', 'dose', 'pill_id', 'pill', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_dose_taper_taper', 'dose', 'taper_id', 'taper', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_dose_user_user', 'dose', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('dose');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
