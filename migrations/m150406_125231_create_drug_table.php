<?php

use yii\db\Schema;
use yii\db\Migration;

class m150406_125231_create_drug_table extends Migration
{
    public function up()
    {
        $this->createTable('drug', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'nick' => Schema::TYPE_STRING,
            'halflife_low' => Schema::TYPE_FLOAT,
            'halflife_high' => Schema::TYPE_FLOAT,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER
        ]);

        $this->addForeignKey('fk_drug_user_user', 'drug', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('drug');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
