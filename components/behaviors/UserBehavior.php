<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components\behaviors;

use Yii;
use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;

class UserBehavior extends AttributeBehavior {
    
    /**
     *
     * @var string the user Id attribute to update with the current users Id
     */
    public $userIdAttribute = 'user_id';
    
    /**
     *
     * @var integer the user Id of the current user
     *      defaults to Yii::$app->user->id
     */
    public $userId = null;
    
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => $this->userIdAttribute,
            ];
        }
    }
    
    /**
     * @inheritdoc
     */
    protected function getValue()
    {
        return $this->userId !== null 
                ? $this->userId
                : Yii::$app->user->id;
    }
    
    /**
     * Updates a user Id attribute to the current users Id.
     *
     * ```php
     * $model->touch('user_id');
     * ```
     * @param string $attribute the name of the attribute to update.
     */
    public function touch($attribute)
    {
        $this->owner->updateAttributes(array_fill_keys((array) $attribute, $this->getValue()));
    }

}
