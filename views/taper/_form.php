<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Taper */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taper-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'drug_id')->textInput() ?>

    <?= $form->field($model, 'start_dose')->textInput() ?>

    <?= $form->field($model, 'end_dose')->textInput() ?>

    <?= $form->field($model, 'step_size')->textInput() ?>

    <?= $form->field($model, 'step_freq')->textInput() ?>

    <?= $form->field($model, 'start_date')->textInput() ?>

    <?= $form->field($model, 'end_date')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
