<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\datecontrol\DateControl; 
/* @var $this yii\web\View */
/* @var $model app\models\Dose */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dose-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pill_id')->textInput() ?>

    <?= $form->field($model, 'taper_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <label class="control-label">Date</label>
    <?= $form->field($model, 'date')->widget(DateControl::className(),[
            'type' => DateControl::FORMAT_DATE,
            'displayFormat' => 'php:d/m/Y', 
            'saveFormat' => 'php:Y-m-d',
            'readonly' => true, 
            'options' => [                  
                'pluginOptions' => [
                    'autoclose' => false,           
                    'todayHighlight' => true,
                ],
            ],
    ]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
